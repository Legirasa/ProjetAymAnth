<?php

  function getListByName($name){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $result = $file_db->query('SELECT * from FILMS where titre like \'%'.$name.'%\'');
    }
    catch(PDOException $e){
    echo $e->getMessage();
  }
  return $result;
}

function getListByRea($name){
  try{
    //Creation de la base SQLite
    $file_db = new PDO('sqlite:films.sqlite3');
    //Gerer le niveau des erreurs rapportées
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = $file_db->query('SELECT * from FILMS where idFilm in(SELECT idFilm from REALISATEUR natural join AREALISATEUR where nomRea like \'%'.$name.'%\')');
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }
  return $result;
  }

function getList(){
  try{
    //Creation de la base SQLite
    $file_db = new PDO('sqlite:films.sqlite3');
    //Gerer le niveau des erreurs rapportées
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = $file_db->query('SELECT * from FILMS');
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  return $result;
}

  function getListByGender($name){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $result = $file_db->query('SELECT * from FILMS where idFilm in(SELECT idFilm from GENRE natural join AGENRE where nomGenre like \'%'.$name.'%\')');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }

  function getIdReaSuivant(){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $result = $file_db->query('SELECT max(idRea) from REALISATEUR');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    foreach ($result as $key) {
      return $key['max(idRea)'] +1;
    }
  }
  function getIdFilmSuivant(){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $result = $file_db->query('SELECT max(idFilm) from FILMS');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    foreach ($result as $key) {
      return $key['max(idFilm)'] +1;
    }
  }

  function addRealisateur($prenom, $nom, $date, $nation){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $sql = 'INSERT INTO REALISATEUR VALUES (:id,:nom,:prenom,:datenaiss,:nat)';
      $id = getIdReaSuivant();
      $stmt=$file_db->prepare($sql);
      $stmt->bindParam(':id', $id);
      $stmt->bindParam(':nom', $nom);
      $stmt->bindParam(':prenom', $prenom);
      $stmt->bindParam(':datenaiss', $date);
      $stmt->bindParam(':nat', $nation);
      $stmt->execute();

    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
  }

  function addFilm($titre, $date, $affiche, $idsRea, $idsGenre, $duree){
    try{
      $idF = getIdFilmSuivant();
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $sql = 'INSERT INTO FILMS VALUES (:id,:titre,:dateFilm,:affiche,:duree)';
      $stmt=$file_db->prepare($sql);
      $stmt->bindParam(':id', $idF);
      $stmt->bindParam(':titre', $titre);
      $stmt->bindParam(':dateFilm', $date);
      $stmt->bindParam(':affiche', $affiche);
      $stmt->bindParam(':duree', $duree);
      $stmt->execute();

      $sql = 'INSERT INTO AREALISATEUR VALUES (:idR, :idF)';
      $stmt=$file_db->prepare($sql);
      $stmt->bindParam(':idR', $idsRea);
      $stmt->bindParam(':idF', $idF);
      $stmt->execute();

      foreach($idsGenre as $id){
        $sql = 'INSERT INTO AGENRE VALUES (:idG, :idF)';
        $stmt=$file_db->prepare($sql);
        $stmt->bindParam(':idG', $id);
        $stmt->bindParam(':idF', $idF);
        $stmt->execute();
      }

    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
  }

  function getIdFilm($titre){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $result = $file_db->query('SELECT idFilm from FILMS where titre = \''.$titre.'\'');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }



  function getReaFilm($titre){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $idF = getIdFilm($titre);
      foreach ($idF as $key) {
        $idFilm = $key['idFilm'];
      }
      $result = $file_db->query('SELECT * from REALISATEUR where idRea in(SELECT idRea from AREALISATEUR where idFilm = '.$idFilm.')');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }

  function getGenreFilm($titre){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $idF = getIdFilm($titre);
      foreach ($idF as $key) {
        $idFilm = $key['idFilm'];
      }
      $result = $file_db->query('SELECT * from GENRE where idGenre in(SELECT idGenre from AGENRE where idFilm = \''.$idFilm.'\')');    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }

  function getListRea(){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $file_db->query('SELECT * from REALISATEUR');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }

  function delFilm($idFilm){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $file_db->query('DELETE from AREALISATEUR where idFilm = '.$idFilm);
      $result = $file_db->query('DELETE from AGENRE where idFilm = '.$idFilm);
      $result = $file_db->query('DELETE from FILMS where idFilm = '.$idFilm);
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
  }

  function recherche($name){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $file_db->query('SELECT * from FILMS where titre like \'%'.$name.'%\' union SELECT * from FILMS where idFilm in(SELECT idFilm from REALISATEUR natural join AREALISATEUR where nomRea like \'%'.$name.'%\' or prenomRea like \'%'.$name.'%\') union SELECT * from FILMS where idFilm in(SELECT idFilm from GENRE natural join AGENRE where nomGenre like \'%'.$name.'%\')');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }

  function getListGenre(){
    try{
      //Creation de la base SQLite
      $file_db = new PDO('sqlite:films.sqlite3');
      //Gerer le niveau des erreurs rapportées
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $file_db->query('SELECT * from GENRE');
    }
    catch(PDOException $e){
      echo $e->getMessage();
    }
    return $result;
  }
 ?>
